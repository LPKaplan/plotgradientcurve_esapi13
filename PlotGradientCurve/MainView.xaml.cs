﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.Win32;
using OxyPlot.Wpf;
using OxyPlot;
using OxyPlot.Pdf;
using VMS.TPS.Common.Model.Types;
using VMS.TPS.Common.Model.API;

namespace PlotGradientCurve
{
    /// <summary>
    /// Interaction logic for MainView.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        // Fields
        private readonly MainViewModel _vm;

        // Constructor
        public MainView(MainViewModel mainViewModel)
        {
            _vm = mainViewModel;

            InitializeComponent();
            DataContext = mainViewModel;

            // Load variables from previous session
            LoadSavedVariables();

            // Show Axes
            _vm.ShowAxes();
        }

        // Methods

        // Load input variable from last session
        private void LoadSavedVariables()
        {
            _vm.NumericInput = Properties.Settings.Default.numericInput;
            NumericInputBox.Text = Properties.Settings.Default.numericInput.ToString();
        }

        #region Structure selection logic
        // Set comboBox selection at startup
        private void FindSelectedStructure(object comboBoxObject, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)comboBoxObject;
            // read input from file, if it exists
            if (_vm.Structures.Any(s => s.Id == Properties.Settings.Default.chosenStructure))
            {
                comboBox.SelectedItem = _vm.Structures.FirstOrDefault(s => s.Id == Properties.Settings.Default.chosenStructure);
            }
            else
            {
                // Let the user know what went wrong.
                MessageBox.Show(string.Format("No structure that matches last used Id. Showing first structure in the set."));

                comboBox.SelectedItem = _vm.Structures.First();
            }
        }

        // Upon change of selection
        private void StructureSelected(object comboBoxObject, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all series from plot
            _vm.RemoveAllSeries();

            var structure = GetStructure(comboBoxObject);
            string Id = structure.Id;
            _vm.ChosenStructureId = Id;

            // Save selected value to file
            Properties.Settings.Default.chosenStructure = Id;
        }

        private Structure GetStructure(object comboBoxObject)
        {
            var comboBox = (ComboBox)comboBoxObject;
            var structure = (Structure)comboBox.SelectedItem;
            return structure;
        }
        #endregion

        #region Plan selection logic
        private void Plan_OnChecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.AddPlanItem(GetPlan(checkBoxObject));
        }

        private void Plan_OnUnchecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.RemovePlanItem(GetPlan(checkBoxObject));
        }

        private PlanSetup GetPlan(object checkBoxObject)
        {
            var checkbox = (CheckBox)checkBoxObject;
            var planName = (PlanName)checkbox.DataContext;
            var plan = _vm.PlansInScope.FirstOrDefault(p => p.Id == planName.Name);
            return plan;
        }
        #endregion

        #region Optional input logic


        private void InputButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry = NumericInputBox.Text;
            if (IsDecimal(entry))
            {
                _vm.NumericInput = Convert.ToDouble(entry);
                MessageBox.Show(string.Format("Input value set to {0}", entry));

                // Save current value
                Properties.Settings.Default.numericInput = Convert.ToDouble(entry);
            }
            else
                MessageBox.Show("Input must be a decimal value!");
        }

        private void LoadReferenceCurveClick(object sender, RoutedEventArgs e)
        {
            string filepath = GetRefCurvePath();
            _vm.PlotRefCurve(filepath);
        }

        private void RemoveReferenceCurveClick(object sender, RoutedEventArgs e)
        {
            _vm.RemoveRefCurves();
        }

        private bool IsDecimal(string input)
        {
            Decimal dummy;
            return Decimal.TryParse(input, out dummy);
        }
        #endregion

        #region Export logic
        private void ExportPlotAsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.ExportPlotAsPdf(filePath);
        }

        private void ExportDataAsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.ExportDataAsTxt(filePath);
        }

        private string GetPdfSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export to PDF",
                Filter = "PDF Files (*.pdf)|*.pdf"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetTxtSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to txt file",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetRefCurvePath()
        {
            Microsoft.Win32.OpenFileDialog loadFileDialog = new OpenFileDialog
            {
                Title = "Choose a file:",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = loadFileDialog.ShowDialog();

            if (dialogResult == true)
                return loadFileDialog.FileName;
            else
                return null;
        }
        #endregion


    }
}
