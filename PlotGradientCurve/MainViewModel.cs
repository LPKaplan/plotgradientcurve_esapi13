﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using win = System.Windows;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using OxyPlot.Annotations;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace PlotGradientCurve
{
    public class MainViewModel
    {
        #region Properties
        public IEnumerable<Structure> Structures { get; private set; }
        public IEnumerable<PlanSetup> PlansInScope { get; private set; }
        public IEnumerable<PlanName> PlanCollection { get; set; }
        public PlotModel PlotModel { get; private set; }
        public string ChosenStructureId { get; set; }
        public double NumericInput { get; set; }
        #endregion

        // Constructor
        public MainViewModel(ScriptContext context)
        {
            // Set properties
            PlansInScope = GetPlansInScope(context);
            Structures = GetPlanStructures(context);
            PlanCollection = GetPlanCollection(PlansInScope);
            PlotModel = CreatePlotModel();
        }

        #region Private methods for setting properties
        // Get a list of all plans in the current context
        private IEnumerable<PlanSetup> GetPlansInScope(ScriptContext context)
        {
            var plans = context.PlansInScope != null
                ? context.PlansInScope : null;

            return plans.Where(p => p.IsDoseValid);
        }

        // Get a list of all structures in the currently loaded structure set
        private IEnumerable<Structure> GetPlanStructures(ScriptContext context)
        {
            var plan = context.PlanSetup != null ? context.PlanSetup : null;
            return plan.StructureSet != null
                ? plan.StructureSet.Structures : null;
        }

        // Make list of PlanName objects. The PlanName class implements INotifyPropertyChanged. 
        // This is so that the script can tell if a plan is checked or unchecked. 
        // Used to uncheck all plans when switching between plot views.
        private IEnumerable<PlanName> GetPlanCollection(IEnumerable<PlanSetup> plansInScope)
        {
            var List = new List<PlanName>();
            foreach (var plan in plansInScope)
            {
                var planName = new PlanName() { Name = plan.Id };
                List.Add(planName);
            }
            return List;
        }

        // Create an empty PlotModel
        private PlotModel CreatePlotModel()
        {
            PlotModel plotModel = new PlotModel();
            plotModel.Background = OxyColors.LightSlateGray;
            plotModel.PlotAreaBackground = OxyColors.White;
            return plotModel;
        }
        #endregion

        #region Private methods for manipulating the PlotModel

        // Find a plan series
        private OxyPlot.Series.Series FindSeries(string id)
        {
            return PlotModel.Series.FirstOrDefault(x =>
            x.Tag.ToString().Contains(id));
        }

        // Find an annotation (line or text in the plot)
        private OxyPlot.Annotations.Annotation FindAnnotation(string id)
        {
            return PlotModel.Annotations.FirstOrDefault(x =>
            (string)x.Tag == id);
        }

        // Find index of plan in PlansInScope, used for placement of boxes and bars on the x-axis
        private double PlanIndex(PlanSetup plan)
        {
            List<PlanSetup> PlanList = PlansInScope.ToList();
            string id = plan.Id;

            return (double)PlanList.FindIndex(pln => pln.Id == id);
        }

        // Update plot when data is added or removed
        private void UpdatePlot()
        {
            PlotModel.InvalidatePlot(true);
        }
        #endregion

        #region Public general methods

        // Set up the correct axes
        public void ShowAxes()
        {
            // Add two linear axes
            OxyPlot.Axes.LinearAxis xaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "Dose [Gy]",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Bottom,
                Key = "xaxis"
            };

            OxyPlot.Axes.LinearAxis yaxis = new OxyPlot.Axes.LinearAxis
            {
                Title = "Effective distance to 100% isodose",
                TitleFontSize = 18,
                TitleFontWeight = FontWeights.Bold,
                AxisTitleDistance = 15,
                FontSize = 16,
                MajorGridlineStyle = LineStyle.Solid,
                MinorGridlineStyle = LineStyle.Solid,
                Position = AxisPosition.Left,
                Key = "yaxis"
            };

            PlotModel.Axes.Add(xaxis);
            PlotModel.Axes.Add(yaxis);

            // Add a legend
            PlotModel.LegendPosition = LegendPosition.RightTop;
            PlotModel.LegendPlacement = LegendPlacement.Outside;
            PlotModel.LegendBorder = OxyColors.Black;
            PlotModel.LegendBackground = OxyColors.LightGray;

            // Set a title
            PlotModel.Title = "Dose gradient curve";
        }

        // Add series when a plan is checked
        public void AddPlanItem(PlanSetup plan)
        {
            // If no structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please define the BODY structure in the drop-down menu.");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a structure with the chosen ID, or the structure is empty.");
                return;
            }
            // If no dose is calculated
            else if (plan.Dose == null)
            {
                win.MessageBox.Show("The plan has no calculated dose.");
                return;
            }
            // If everything is OK add the correct series type to the plot
            else
            {
                // Ensure dose is given in Gy
                plan.DoseValuePresentation = DoseValuePresentation.Absolute;

                Structure structure = plan.StructureSet.Structures.First(s => s.Id == ChosenStructureId);

                PlotModel.Series.Add(CreateGradientCurveSeries(plan, structure));

                UpdatePlot();
            }
        }

        // Method to plot a reference curve from a txt file
        public void PlotRefCurve(string filePath)
        {
            List<DataPoint> refCurve = LoadCurveFromFile(filePath);

            if (refCurve != null)
            {
                var series = new OxyPlot.Series.LineSeries();
                series.Tag = "Reference";
                series.Title = "Reference";

                series.Points.AddRange(refCurve);
                PlotModel.Series.Add(series);

                UpdatePlot();
            }
            else
            {
                //win.MessageBox.Show("File contents format incorrect.");
                return;
            }
        }

        public void RemoveRefCurves()
        {
            while (PlotModel.Series.Any(ser => (string)ser.Tag == "Reference"))
            {
                var series = FindSeries("Reference");
                PlotModel.Series.Remove(series);
            }

            UpdatePlot();
        }

        // Remove series from plot when box is unchecked
        public void RemovePlanItem(PlanSetup plan)
        {
            while (PlotModel.Series.Any(ser => (string)ser.Tag == plan.Id))
            {
                var series = FindSeries(plan.Id);
                PlotModel.Series.Remove(series);
            }
            while (PlotModel.Annotations.Any(ser => (string)ser.Tag == plan.Id))
            {
                var annotation = FindAnnotation(plan.Id);
                PlotModel.Annotations.Remove(annotation);
            }
            UpdatePlot();
        }

        // Clear previous plot model
        public void OnClear()
        {
            PlotModel.Axes.Clear();
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Clear all plot series when input is changed
        public void RemoveAllSeries()
        {
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Export plot as pdf
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                PdfExporter.Export(PlotModel, stream, 600, 400);
            }
        }

        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModel.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LineSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.LinearBarSeries().GetType()) | OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.AreaSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        sw.WriteLine(string.Format("#{0}\t{1}", PlotModel.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Bottom).Title, PlotModel.Axes.FirstOrDefault(ax => ax.Position == AxisPosition.Left).Title));
                        var dataSer = (OxyPlot.Series.DataPointSeries)ser;
                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X.ToString(), point.Y.ToString()));
                        }
                        sw.WriteLine(string.Format("\n\n"));
                    }
                }
            }
        }

        
        #endregion

        #region Create series methods

        // Create the oxyplot series
        private OxyPlot.Series.Series CreateGradientCurveSeries(PlanSetup plan, Structure body)
        {
            var series = new OxyPlot.Series.LineSeries();
            series.Tag = plan.Id;
            series.Title = plan.Id;

            DVHData bodyDVH = plan.GetDVHCumulativeData(body, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            for (double d = 0.5; d < NumericInput; d += 0.1)
            {
                var val = CalculateGradientIndex(plan, body, bodyDVH, NumericInput, d);
                var point = new DataPoint(d, val);
                series.Points.Add(point);
            }

            return series;
        }

        // Calculate the GI
        private double CalculateGradientIndex(PlanSetup plan, Structure body, DVHData bodyDVH, double prescriptionDoseLevel, double evalDoseLevel)
        {
            double VX;
            double V100;

            // For imported doses the dose coverage of the body volume can be less than 100%. In this case the GetVolumeAtDose method returns NaN. The VolumeFromDVH method is slightly less accurate, but it works in these cases.
            if (bodyDVH.Coverage < 1.0)
            {
                VX = VolumeFromDVH(bodyDVH, evalDoseLevel);
                V100 = VolumeFromDVH(bodyDVH, prescriptionDoseLevel);
            }
            // if dose coverage is 100%
            else
            {
                VX = plan.GetVolumeAtDose(body, new DoseValue(evalDoseLevel, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
                V100 = plan.GetVolumeAtDose(body, new DoseValue(prescriptionDoseLevel, DoseValue.DoseUnit.Gy), VolumePresentation.AbsoluteCm3);
            }
            var EffRadX = Math.Pow((3.0 / 4.0) * Math.PI, (1.0 / 3.0)) * Math.Pow(VX, (1.0 / 3.0));
            var EffRad100 = Math.Pow((3.0 / 4.0) * Math.PI, (1.0 / 3.0)) * Math.Pow(V100, (1.0 / 3.0));

            return (EffRadX - EffRad100);
        }

        // Helper method in case DVH calculation coverage is too low


        /// <summary>
        /// Interpolates dvh curve to return volume for a given dose
        /// If the requested dose is higher than the last item in the DVH curve then none of the contour
        /// receives this dose and 0 is returned.
        /// </summary>
        /// <param name="dvh"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static double VolumeFromDVH(DVHData dvh, double value)
        {
            double ans;
            List<DVHPoint> curve = dvh.CurveData.ToList();
            if (value <= curve.Last().DoseValue.Dose)
            {
                int index = curve.IndexOf(curve.First(i => i.DoseValue.Dose >= value));
                ans = curve[index - 1].Volume + (value - curve[index - 1].DoseValue.Dose) *
                (curve[index].Volume - curve[index - 1].Volume) /
                (curve[index].DoseValue.Dose - curve[index - 1].DoseValue.Dose);
            }
            else
            {
                ans = 0;
            }
            return ans;
        }
        #endregion

        #region Other helper methods
        private List<DataPoint> LoadCurveFromFile(string filePath)
        {
            // initialize return variable
            List<DataPoint> points = new List<DataPoint>();

            // read file line by line and check syntax for each line
            string line;
            double dose;
            double val;

            
            using (StreamReader sr = new StreamReader(filePath))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    if (line == String.Empty) // ignore commented and empty lines
                    {
                        continue;
                    }
                    else if (line[0] == '#')
                    {
                        continue;
                    }
                    else
                    {
                        string[] splitLine = line.Split();

                        if (splitLine.Length == 2 && double.TryParse(splitLine[0], out dose) && double.TryParse(splitLine[1], out val))
                        {
                            points.Add(new DataPoint(dose, val));
                        }
                        else // if a line with incorrect format is encountered return null
                        {
                            win.MessageBox.Show("Something's wrong with the file. Check the line format is %d\t%d.");
                            return null;
                        }
                    }
                }
            }
            
            return points;
        }
        #endregion
    }
}
